import xml.etree.ElementTree as ET
import os

classes = ["wild-boar"]

VOCPath = "/content/drive/My Drive/wild-boar-yolov3/main/output/PASCAL_VOC"
imgPath = "/content/drive/My Drive/wild-boar-yolov3/main/input"

list_file = open("/content/drive/My Drive/wild-boar-yolov3/main/VOC_wildboar.txt", "w")

for filename in os.listdir(VOCPath):
    print("Processing on file {}".format(filename))
    image_id = filename.split(".")[0]
    in_file = open("{}/{}.xml".format(VOCPath, image_id))
    tree = ET.parse(in_file)
    root = tree.getroot()

    for obj in root.iter('object'):
        difficult = obj.find('difficult').text
        cls = obj.find('name').text
        if cls not in classes or int(difficult)==1:
            continue
        cls_id = classes.index(cls)
        xmlbox = obj.find('bndbox')
        b = (int(xmlbox.find('xmin').text), int(xmlbox.find('ymin').text), int(xmlbox.find('xmax').text), int(xmlbox.find('ymax').text))
        list_file.write('{}/{}.jpg'.format(imgPath, image_id))
        list_file.write(" " + ",".join([str(a) for a in b]) + ',' + str(cls_id))
        list_file.write('\n')

list_file.close()