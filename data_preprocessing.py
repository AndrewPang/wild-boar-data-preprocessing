import os

darknetPath = "main/output/YOLO_darknet"
VOCPath = "main/output/PASCAL_VOC"
imgPath = "main/input"

for filename in os.listdir(darknetPath):
    try:
        if os.path.getsize("main/output/YOLO_darknet/{}".format(filename)) == 0:
            emptyFileName = filename.split(".")[0]
            # Remove txt file in YOLO_darnet directory
            os.remove("{}/{}".format(darknetPath, filename))
            # Remove xml file in VOCPath directory
            os.remove("{}/{}.xml".format(VOCPath, emptyFileName))
            # Remove img file in imgPath directory
            os.remove("{}/{}.jpg".format(imgPath, emptyFileName))
            print("Deleting empty file {}".format(emptyFileName))
    except OSError as e:
        print(e)